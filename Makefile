GOARCH = amd64
GOOS ?= linux
BINARY = pre-install-check

BINARY_NAME_FULL_NAME = $(BINARY)_$(GOOS)_$(GOARCH)
BUILD_FLAGS = GO111MODULE=on CGO_ENABLED=0 GOOS=$(GOOS) GOARCH=$(GOARCH) GOFLAGS=-mod=vendor
GO_BUILD = $(BUILD_FLAGS) go build $(GO_LD_FLAGS) -o $(BINARY_NAME_FULL_NAME) $(VERBOSE_FLAG) $(GOTARGET)

GOLINT_VERSION:=1.19.1
GOLINT_INSTALLED = $(shell golangci-lint --version | grep -q "$(GOLINT_VERSION)" && echo "true" || echo "false" )
ifeq ($(GOLINT_INSTALLED),true)
GOLINT_EXEC:=golangci-lint
else
IGNORE:=$(shell curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s -- -b $$(go env GOPATH)/bin v$(GOLINT_VERSION))
GOLINT_EXEC:=$$(go env GOPATH)/bin/golangci-lint
endif

# vars used for testing only
REMOTE := ec2-user@54.198.246.212
PEMKEY := ~/.ssh/k8s-staging.pem
TESTFILE := pre-install_test.txt

all: lint build

lint:
	@echo Running linters...
	$(BUILD_FLAGS) $(GOLINT_EXEC) run

build:
	$(GO_BUILD)

test:
	$(info Make: Test execution on remote test system via scp - creates $(TESTFILE))
	@rsync -az -e "ssh -i $(PEMKEY)" $(BINARY_NAME_FULL_NAME) $(REMOTE):.
	@ssh -i $(PEMKEY) $(REMOTE) "./$(BINARY_NAME_FULL_NAME) > $(TESTFILE) 2>&1"
	@rsync --remove-source-files -az -e "ssh -i $(PEMKEY)" $(REMOTE):$(TESTFILE) .
	@ls -l $(TESTFILE)

clean:
	@rm -f $(BINARY_NAME_FULL_NAME) $(TESTFILE)