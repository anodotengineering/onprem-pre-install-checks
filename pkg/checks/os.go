package check

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"log"
	"net"
	"os"
	"os/exec"
	initsystem2 "pre-install-check/pkg/utils/initsystem"
	"strconv"
	"strings"
	"sync"
)

var (
	ErrAufsNotSupported = fmt.Errorf("AUFS was not found in /proc/filesystems")
	requiredOpenPorts   = []uint64{2379, 2380, 9099, 6443, 10250, 10251, 10252}
)

//Kernel version

type DockerAUFSStorage struct {
}

func (d DockerAUFSStorage) Run(r chan Result, wg *sync.WaitGroup) {
	defer wg.Done()

	err := supportsAufs()

	if err != nil {
		r <- Result{
			Name:    "Docker AUFS storage supported",
			Passed:  false,
			Message: fmt.Sprintf("AUFS storage not supported: %s", err.Error()),
		}
	} else {
		r <- Result{
			Name:    "Docker AUFS storage supported",
			Passed:  true,
			Message: "OK",
		}
	}
}

func supportsAufs() error {
	// We can try to modprobe aufs first before looking at
	// proc/filesystems for when aufs is supported
	err := exec.Command("modprobe", "aufs").Run()
	if err != nil {
		return err
	}

	f, err := os.Open("/proc/filesystems")
	if err != nil {
		return err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		if strings.Contains(s.Text(), "aufs") {
			return nil
		}
	}
	return ErrAufsNotSupported
}

type OpenPorts struct {
	requiredPorts []uint64
}

//443 or 80 add
func NewOpenPortsCheck() OpenPorts {
	return OpenPorts{requiredPorts: requiredOpenPorts}
}

func (o OpenPorts) Run(r chan Result, wg *sync.WaitGroup) {
	defer wg.Done()

	var errors []error
	for _, port := range o.requiredPorts {
		err := listenOnPort(port)
		if err != nil {
			errors = append(errors, err)
		}
	}

	if len(errors) == 0 {
		s, _ := json.Marshal(o.requiredPorts)
		r <- Result{
			Name:    "Required Ports Opened",
			Passed:  true,
			Message: fmt.Sprintf("Ports (%s) opened.", s),
		}
	} else {
		r <- Result{
			Name:    "Required Ports Opened",
			Passed:  false,
			Message: fmt.Sprintf("Failed with errors: %s", errors),
		}
	}

}

func listenOnPort(port uint64) error {
	conn, err := net.Listen("tcp", ":"+strconv.Itoa(int(port)))
	if conn != nil {
		conn.Close()
		return nil
	}
	return err
}

// FileContentCheck checks that the given file contains the string Content.
type FileContentCheck struct {
	Path    string
	Label   string
	Comment string
	Content []byte
}

// Name returns label for individual FileContentChecks. If not known, will return based on path.
func (fcc FileContentCheck) Name() string {
	if fcc.Label != "" {
		return fcc.Label
	}
	return fmt.Sprintf("FileContent [%q]", fcc.Path)
}

// Check validates if the given file contains the given content.
func (fcc FileContentCheck) Run(r chan Result, wg *sync.WaitGroup) {

	defer wg.Done()

	log.Println(fmt.Sprintf("validating the contents of file %s", fcc.Path))
	f, err := os.Open(fcc.Path)
	if err != nil {
		r <- Result{
			Name:    fcc.Name(),
			Passed:  false,
			Message: fmt.Sprintf("%s does not exist", fcc.Path),
		}
		return
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {

		// skip comment lines
		l := len(fcc.Comment)
		if l > 0 {
			if string(scanner.Bytes()[0:l]) == fcc.Comment {
				continue
			}
		}

		if bytes.Contains(scanner.Bytes(), fcc.Content) {
			r <- Result{
				Name:    fcc.Name(),
				Passed:  true,
				Message: fmt.Sprintf("OK. Contains: %s", fcc.Content),
			}
			return
		}
	}
	if err := scanner.Err(); err != nil {
		r <- Result{
			Name:    fcc.Name(),
			Passed:  false,
			Message: fmt.Sprintf("%s could not be read", fcc.Path),
		}
	}

	r <- Result{
		Name:    fcc.Name(),
		Passed:  false,
		Message: fmt.Sprintf("%s contents are not set to %s", fcc.Path, fcc.Content),
	}

}

// FirewalldCheck checks if firewalld is enabled or active. If it is, warn the user that there may be problems
// if no actions are taken.
type FirewalldCheck struct {
}

// Name returns label for FirewalldCheck.
func (FirewalldCheck) Name() string {
	return "Firewalld"
}

// Check validates if the firewall is enabled and active.
func (fc FirewalldCheck) Run(r chan Result, wg *sync.WaitGroup) {
	defer wg.Done()

	log.Println("validating if the firewall is enabled and active")
	initSystem, err := initsystem2.GetInitSystem()
	if err != nil {
		r <- Result{
			Name:    fc.Name(),
			Passed:  false,
			Message: err.Error(),
		}
		return
	}

	if !initSystem.ServiceExists("firewalld") {
		r <- Result{
			Name:    fc.Name(),
			Passed:  true,
			Message: "OK. firewalld does not exists",
		}

		return
	}

	if initSystem.ServiceIsActive("firewalld") {
		err := errors.Errorf("firewalld is active, please ensure ports %v are open or your cluster may not function correctly",
			requiredOpenPorts)

		r <- Result{
			Name:    fc.Name(),
			Passed:  false,
			Message: err.Error(),
		}

		return
	}

	r <- Result{
		Name:    fc.Name(),
		Passed:  true,
		Message: "OK",
	}
}

// InPathCheck checks if the given Executable is present in $PATH
type InPathCheck struct {
	Executable string
}

// Name returns label for individual InPathCheck. If not known, will return based on path.
func (ipc InPathCheck) Name() string {
	return fmt.Sprintf("ExectuableInPath [%q]", strings.Replace(ipc.Executable, "/", "-", -1))
}

// Check validates if the given Executable is present in the path.
func (ipc InPathCheck) Run(r chan Result, wg *sync.WaitGroup) {
	defer wg.Done()

	log.Println(fmt.Sprintf("validating the presence of Executable %s", ipc.Executable))
	_, err := exec.LookPath(ipc.Executable)
	if err != nil {
		r <- Result{
			Name:    ipc.Name(),
			Passed:  false,
			Message: fmt.Sprintf("%s not found in system path", ipc.Executable),
		}

		return

	}

	r <- Result{
		Name:    ipc.Name(),
		Passed:  true,
		Message: fmt.Sprintf("OK. %q is present", ipc.Executable),
	}
}

type DNSSearchDomains struct {
}

func DNSSearchDomainsCheck() DNSSearchDomains {
	return DNSSearchDomains{}
}

func (d DNSSearchDomains) Run(r chan Result, wg *sync.WaitGroup) {
	defer wg.Done()

	file, err := os.Open("/etc/resolv.conf")
	if err != nil {
		r <- Result{
			Name:    "Domain Search List Check",
			Passed:  false,
			Message: fmt.Sprintf("Unable to process DNS resolver file"),
		}
	}
	defer file.Close()

	var domains []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "search") {
			d := strings.TrimPrefix(scanner.Text(), "search")
			domains = strings.Fields(d)
		}
	}

	// how many domains
	count := len(domains)

	// max allowed by k8s and kubespray install process
	const maxSearchEntries = 5

	if count <= maxSearchEntries {
		r <- Result{
			Name:    fmt.Sprintf("Domain Search List <= %d entries", maxSearchEntries),
			Passed:  true,
			Message: fmt.Sprintf("Search Domains: %s (%d)", domains, count),
		}
	} else {
		r <- Result{
			Name:    fmt.Sprintf("Domain Search List > %d entries", maxSearchEntries),
			Passed:  false,
			Message: fmt.Sprintf("Search Domains: %s (%d)", domains, count),
		}
	}
}
