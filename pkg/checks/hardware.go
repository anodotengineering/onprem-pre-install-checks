package check

import (
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/shirou/gopsutil/mem"
	"runtime"
	"sync"
)

type MemoryCheck struct {
	requiredSizeInBytes uint64
}

func (m MemoryCheck) Run(r chan Result, wg *sync.WaitGroup) {
	defer wg.Done()

	memStat, e := mem.VirtualMemory()
	if e != nil {
		r <- Result{
			Name:    "Free Memory Check",
			Passed:  false,
			Message: fmt.Sprintf("Failed to execute RAM size check:%s", e.Error()),
		}
		return
	}

	if memStat.Total < m.requiredSizeInBytes {
		r <- Result{
			Name:    "Free Memory Check",
			Passed:  false,
			Message: fmt.Sprintf("Required size=%s, actual size=%s.", humanize.IBytes(m.requiredSizeInBytes), humanize.IBytes(memStat.Total)),
		}
		return
	} else {
		r <- Result{
			Name:    "Free Memory Check",
			Passed:  true,
			Message: fmt.Sprintf("OK. Memory size is=%s", humanize.IBytes(memStat.Total)),
		}
		return
	}
}

func NewMemoryCheck(requiredSizeInBytes uint64) MemoryCheck {
	return MemoryCheck{requiredSizeInBytes: requiredSizeInBytes}
}

// NumCPUCheck checks if current number of CPUs is not less than required
type NumCPUCheck struct {
	NumCPU int
}

// Name returns the label for NumCPUCheck
func (NumCPUCheck) Name() string {
	return "NumCPU"
}

// Check number of CPUs required by kubeadm
func (ncc NumCPUCheck) Run(r chan Result, wg *sync.WaitGroup) {
	defer wg.Done()

	numCPU := runtime.NumCPU()
	if numCPU < ncc.NumCPU {
		r <- Result{
			Name:    ncc.Name(),
			Passed:  false,
			Message: fmt.Sprintf("the number of available CPUs %d is less than the required %d", numCPU, ncc.NumCPU),
		}
		return
	}

	r <- Result{
		Name:    ncc.Name(),
		Passed:  true,
		Message: fmt.Sprintf("the number of available CPUs %d", numCPU),
	}
}
