package check

import "sync"

type Result struct {
	Name    string
	Passed  bool
	Message string
}

type Runner interface {
	Run(r chan Result, wg *sync.WaitGroup)
}
