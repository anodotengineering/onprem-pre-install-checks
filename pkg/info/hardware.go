package info

import (
	"context"
	"github.com/dustin/go-humanize"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	"log"
	"os/exec"
	"runtime"
)

type Info struct {
	CPU    CPU
	Memory Memory
	//	Storage Storage
	LoadAvg LoadAvg
}

type CPU struct {
	ModelName string
	Cores     int32
	Threads   int
}

type Memory struct {
	Total string
}

/*type Storage struct {
	Disks []Disks
}

type Disks struct {
	Name                   string
	Size                   string
	PhysicalBlockSizeBytes uint64

	Partitions []Partitions
}
type Partitions struct {
	Name       string
	MountPoint string
	IsReadOnly bool
	Size       string
	Type       string
}
*/
type LoadAvg struct {
	Load1  float64
	Load5  float64
	Load15 float64
}

func HWSpec() (Info, error) {

	log.Println("disk usage information:")
	out, err := execCommand("df", "-h")
	if err != nil {
		log.Println("Failed to execute df command: ", err.Error())
	}
	log.Println("\n" + string(out))

	log.Println("mount information:")
	out, err = execCommand("mount")
	if err != nil {
		log.Println("Failed to execute mount command: ", err.Error())
	}
	log.Println("\n" + string(out))

	log.Println("memory information:")
	out, err = execCommand("free", "-h")
	if err != nil {
		log.Println("Failed to execute free command: ", err.Error())
	}
	log.Println("\n" + string(out))

	/*	block, err := ghw.Block()
		if err != nil {
			fmt.Printf("Error getting block storage info: %v", err)
		}*/

	/*storage := &Storage{}
	for _, disk := range block.Disks {
		d := Disks{
			Name:                   disk.Name,
			Size:                   humanize.IBytes(disk.SizeBytes),
			PhysicalBlockSizeBytes: disk.PhysicalBlockSizeBytes,
			Partitions:             nil,
		}

		for _, partition := range disk.Partitions {
			d.Partitions = append(d.Partitions, Partitions{
				Name:       partition.Name,
				MountPoint: partition.MountPoint,
				IsReadOnly: partition.IsReadOnly,
				Size:       humanize.IBytes(partition.SizeBytes),
				Type:       partition.Type,
			})
		}

		storage.Disks = append(storage.Disks, d)
	}*/

	stats, _ := cpu.InfoWithContext(context.Background())
	memStat, e := mem.VirtualMemory()
	if e != nil {
		return Info{}, e
	}

	stat, e := load.Avg()
	if e != nil {
		return Info{}, e
	}

	m := Memory{Total: humanize.IBytes(memStat.Total)}
	c := CPU{ModelName: stats[0].ModelName, Cores: stats[0].Cores, Threads: runtime.NumCPU()}
	avg := LoadAvg{
		Load1:  stat.Load1,
		Load5:  stat.Load5,
		Load15: stat.Load15,
	}

	return Info{
		CPU:    c,
		Memory: m,
		//Storage: *storage,
		LoadAvg: avg,
	}, nil
}

func execCommand(command string, arg ...string) ([]byte, error) {
	cmd := exec.Command(command, arg...)
	return cmd.Output()
}
