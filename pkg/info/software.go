package info

import (
	"github.com/shirou/gopsutil/host"
)

type Spec struct {
	Hostname             string `json:"hostname"`
	Uptime               uint64 `json:"uptime"`
	BootTime             uint64 `json:"bootTime"`
	Procs                uint64 `json:"procs"`           // number of processes
	OS                   string `json:"os"`              // ex: freebsd, linux
	Platform             string `json:"platform"`        // ex: ubuntu, linuxmint
	PlatformFamily       string `json:"platformFamily"`  // ex: debian, rhel
	PlatformVersion      string `json:"platformVersion"` // version of the complete OS
	KernelVersion        string `json:"kernelVersion"`   // version of the OS kernel (if available)
	KernelArch           string `json:"kernelArch"`      // native cpu architecture queried at runtime, as returned by `uname -m` or empty string in case of error
	VirtualizationSystem string `json:"virtualizationSystem"`
	VirtualizationRole   string `json:"virtualizationRole"` // guest or host
	HostID               string `json:"hostid"`

}

func OSSpec() (Spec, error) {
	stat, e := host.Info()
	if e != nil {
		return Spec{}, e
	}

	spec := Spec{
		Hostname:             stat.Hostname,
		Uptime:               stat.Uptime,
		BootTime:             stat.BootTime,
		Procs:                stat.Procs,
		OS:                   stat.OS,
		Platform:             stat.Platform,
		PlatformFamily:       stat.PlatformFamily,
		PlatformVersion:      stat.PlatformVersion,
		KernelVersion:        stat.KernelVersion,
		KernelArch:           stat.KernelArch,
		VirtualizationSystem: stat.VirtualizationSystem,
		VirtualizationRole:   stat.VirtualizationRole,
		HostID:               stat.HostID,
	}

	return spec, nil
}
