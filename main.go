package main

import (
	"gopkg.in/yaml.v2"
	"log"
	"os"
	checks "pre-install-check/pkg/checks"
	"pre-install-check/pkg/info"
	"strconv"
	"sync"
	"time"

	"github.com/olekukonko/tablewriter"
)

var (
	ipv4Forward   = "/proc/sys/net/ipv4/ip_forward"
	bridgenf      = "/proc/sys/net/bridge/bridge-nf-call-iptables"
	sshdConfig    = "/etc/security/sshd_config"
	accessConfig  = "/etc/security/access.conf"
	hostsAllow    = "/etc/hosts.allow"
	hostDeny      = "/etc/hosts.deny"
)

func main() {

	hwSpec, _ := info.HWSpec()
	out, _ := yaml.Marshal(hwSpec)

	log.Println("Hardware spec:")
	log.Println(string(out))

	osSpec, _ := info.OSSpec()
	specOut, _ := yaml.Marshal(osSpec)
	log.Println("Software spec:")
	log.Println(string(specOut))

	allChecks := make([]checks.Runner, 0)
	allChecks = append(allChecks, checks.NewOpenPortsCheck())
	allChecks = append(allChecks, checks.DockerAUFSStorage{})
	allChecks = append(allChecks, checks.NewMemoryCheck(64*1024*1024*1024))
	allChecks = append(allChecks, checks.NumCPUCheck{NumCPU: 32})
	allChecks = append(allChecks, checks.FileContentCheck{Path: ipv4Forward, Content: []byte{'1'}})
	allChecks = append(allChecks, checks.FileContentCheck{Path: bridgenf, Content: []byte{'1'}})
	allChecks = append(allChecks, checks.FirewalldCheck{})
	allChecks = append(allChecks, checks.DNSSearchDomainsCheck())
	allChecks = append(allChecks, checks.FileContentCheck{Path: sshdConfig, Content: []byte("root:"), Comment: "#"})
	allChecks = append(allChecks, checks.FileContentCheck{Path: accessConfig, Content: []byte("root:"), Comment: "#"})
	allChecks = append(allChecks, checks.FileContentCheck{Path: hostsAllow, Content: []byte("ALL:"), Comment: "#"})
	allChecks = append(allChecks, checks.FileContentCheck{Path: hostDeny, Content: []byte("ALL:"), Comment: "#"})

	allChecks = append(allChecks, checks.InPathCheck{Executable: "ip"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "iptables"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "mount"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "socat"})

	allChecks = append(allChecks, checks.InPathCheck{Executable: "curl"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "sudo"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "rsync"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "unzip"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "e2fsprogs"})
	allChecks = append(allChecks, checks.InPathCheck{Executable: "xfsprogs"})

	checkResults := make(chan checks.Result, len(allChecks))
	wg := &sync.WaitGroup{}
	for _, v := range allChecks {
		wg.Add(1)
		go v.Run(checkResults, wg)
	}

	go func() {
		wg.Wait()
		close(checkResults)
	}()

	var results []checks.Result
INFINITE_LOOP:
	for {
		select {
		case <-time.After(60 * time.Second):
			log.Println("Timeout occurred while executing allChecks...")
			showResults(results)
			os.Exit(1)
		case msg, ok := <-checkResults:
			if !ok {
				break INFINITE_LOOP
			}
			results = append(results, msg)
		}
	}
	showResults(results)
}

func showResults(results []checks.Result) {
	var data [][]string

	for _, r := range results {
		data = append(data, []string{
			r.Name, strconv.FormatBool(r.Passed), r.Message,
		})
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetRowLine(true)
	table.SetRowSeparator("-")
	table.SetHeader([]string{"Name", "Passed", "Notes"})

	for _, v := range data {
		table.Append(v)
	}
	table.Render()
}
