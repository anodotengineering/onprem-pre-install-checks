module pre-install-check

go 1.12

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/jaypipes/ghw v0.0.0-20190630182512-29869ac89830 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/miekg/dns v1.1.29
	github.com/olekukonko/tablewriter v0.0.1
	github.com/pkg/errors v0.8.0
	github.com/shirou/gopsutil v2.19.9+incompatible
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
	golang.org/x/sys v0.0.0-20191008105621-543471e840be // indirect
	gopkg.in/yaml.v2 v2.2.4
)
