Tool is used to check os hardware and software specification based on [this](#https://support.anodot.com/hc/en-us/articles/360033716813-On-Premises-Installation-Guide-V2-2) doc

# Build
`make GOOS=darwin`
Possible GOOS options: [`darwin`,`linux`,`windows`]
Full list: https://golang.org/doc/install/source#environment

# Run
`./pre-install-check_linux_amd64`

# Currently supported checks:

- NumCPU
- FileContent ["/proc/sys/net/ipv4/ip_forward"]
- FileContent ["/proc/sys/net/bridge/bridge-nf-call-iptables"]
- ExecutableInPath ["curl"]
- ExecutableInPath ["xfsprogs"]
- ExecutableInPath ["socat"] 
- Docker AUFS storage supported
- ExecutableInPath ["iptables"]
- ExecutableInPath ["rsync"]
- ExecutableInPath ["ip"]
- ExecutableInPath ["e2fsprogs"]
- ExecutableInPath ["unzip"]
- ExecutableInPath ["sudo"]
- ExecutableInPath ["mount"]
- Firewalld
- Required Ports Opened
- Free Memory Check
- DNS Search Domains Check
- Root Access via SSHD 
- ALL: entries in hosts allow or deny files
